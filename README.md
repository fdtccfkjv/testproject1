# Projects for reqruiters

Hello, and welcome to my GitHub page where I share miniprojects I created during my learning process on the road to become data analyst.

# Files and folders
1. Dag files with some simple dag instructions for airflow
2. Python data exploration notebook with cohort and RMF analysis
3. Delivery service metrics - test project done to ecomerce firm
4. Credit Score Analysis - some data cleanup in python


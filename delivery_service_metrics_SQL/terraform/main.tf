terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = ""
  cloud_id  = "b1gbloksojt8rcdsul37"
  folder_id = "b1gf1f5ptc58lq4irbkm"
  zone      = "ru-central1-b"
}

resource "yandex_vpc_network" "clickhouse_net" { name = "clickhouse_net" }

resource "yandex_vpc_subnet" "clickhouse_subnet" {
  network_id     = yandex_vpc_network.clickhouse_net.id
  name           = "clickhouse_subnet"
  v4_cidr_blocks = ["172.16.1.0/24"]
  zone           = "ru-central1-b"
}

resource "yandex_vpc_security_group" "clickhouse_sg" {
 network_id = yandex_vpc_network.clickhouse_net.id

  ingress {
    description    = "HTTPS (secure)"
    port           = 8443
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description    = "clickhouse-client (secure)"
    port           = 9440
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description    = "Allow all egress cluster traffic"
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "yandex_mdb_clickhouse_cluster" "mych" {
  name               = "mych"
  environment        = "PRESTABLE"
  network_id         = yandex_vpc_network.clickhouse_net.id
  security_group_ids = [yandex_vpc_security_group.clickhouse_sg.id]

  clickhouse {
    resources {
      resource_preset_id = "s2.micro"
      disk_type_id       = "network-ssd"
      disk_size          = 32
    }
  }

  host {
    type      = "CLICKHOUSE"
    zone      = "ru-central1-b"
    subnet_id = yandex_vpc_subnet.clickhouse_subnet.id
  }

  database {
    name = "db1"
  }

  user {
    name     = "user1"
    password = "user1user1"
    permission {
      database_name = "db1"
    }
  }
 access {
    web_sql    = true
  }
}
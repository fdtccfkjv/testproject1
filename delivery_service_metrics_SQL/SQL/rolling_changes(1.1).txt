SELECT
    Month,
    (monthly_sum - lag_sum)/lag_sum as change --find client change rate
FROM(
    SELECT -- prepare the table for final calculation by adding column for previous month clients total for each month
        Month,
        lagInFrame(monthly_sum) OVER (ORDER BY Month ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as lag_sum,
        monthly_sum
    FROM(
            SELECT --calculate distinct users by each month
                toStartOfMonth(order_date) as Month,
                COUNT(DISTINCT client_id) as monthly_sum
            FROM
                db1.task1_1
            GROUP BY
                Month
        )
    ORDER BY
        Month ASC
    )
ORDER BY 
    Month